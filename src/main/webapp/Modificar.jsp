<%-- 
    Document   : index
    Created on : 6 jun. 2021, 13:25:19
    Author     : gerva
--%>

<%@page import="cl.entity.Departamento"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Departamento departamentos=(Departamento)request.getAttribute("departamentos");
    %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="styles/style.css" rel="stylesheet" type="text/css">
    </head>
   </head> 
    <body>
        <form  name="form1" action="DepartamentoController" method="POST">
	    <h3>EDITAR DATOS</h3>
            <div> <br>
                <p type="idDepartamento:"> <input placeholder="Departamento" type="text" name="idDepartamento" value="<%= departamentos.getIdDepartamento()%>" id="idDepartamento" > </p>
                <p type="Piso:"> <input placeholder="Piso"type="text" name="piso" value="<%= departamentos.getPiso()%>" id="piso" > </p>
                <p type="Orientacion:"> <input placeholder= "Orientacion" type="text" name="orientacion" value="<%= departamentos.getOrientacion()%>" id="orientacion" > </p>
                <p type="Dormitorios:"> <input placeholder= "Dormitorios" type="text" name="dormitorios" value="<%= departamentos.getDormitorios()%>" id="dormitorios" > </p>
                <p type="Baños:"> <input placeholder= "Baños" type="text" name="banos" value="<%= departamentos.getBanos()%>" id="banos" > </p>
            </div>        
            
            <button type="submit" name="accion" value="INGRESAR" class="btn btn-success">Modificar</button>
         
        </form>
    </body>
</html>